﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Jimboblion.Anigame.Donations
{
    public class Clan
    {
        [Key]
        public long Id { get; internal set; }
        public string Name { get; internal set; }
        public List<Member> Members { get; internal set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jimboblion.Anigame.Donations
{
    public class Donation
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long ClanId { get; set; }
        public long MemberId { get; set; }
        public DateTime Time { get; set; }
        public long Amount { get; set; }
    }
}
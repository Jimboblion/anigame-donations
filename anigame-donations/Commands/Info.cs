﻿using Jimboblion.Anigame.Donations;
using Discord;
using Discord.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace Jimboblion.Anigame.Donations.Commands
{
    public class InfoModule : ModuleBase<SocketCommandContext>
    {
        [Command("about")]
        [Summary("Shows information about the anigame donations bot")]
        public async Task AboutAsync()
        {
            var builder = new EmbedBuilder();
            builder.Author = new EmbedAuthorBuilder();
            builder.Author.Name = Context.Client.CurrentUser.Username;
            builder.Author.IconUrl = Context.Client.CurrentUser.GetAvatarUrl();
            builder.Title = "About Anigame Donations";
            builder.Description = $"Anigame Donation Tracker version 0.3\r\nStarted {Program.Start}\r\n";
            await ReplyAsync(embed: builder.Build());
        }
    }
}

﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jimboblion.Anigame.Donations.Commands
{
    [Group("dadmin")]
    [RequireOwner]
    public class Admin : ModuleBase<SocketCommandContext>
    {
        public Admin()
        {
        }

        [Group("list")]
        public class List : ModuleBase<SocketCommandContext>
        {
            public List(DonationDatabase database)
            {
                Database = database;
            }

            public DonationDatabase Database { get; }

            [Command("clans")]
            [Summary("Lists all clans in the database")]
            public async Task ListClanAsync()
            {
                var clans = await Database.Clans.ToListAsync();
                StringBuilder output = new StringBuilder();
                foreach (var clan in clans)
                {
                    output.AppendLine($"{clan.Id}: {clan.Name}");
                }
                await ReplyAsync(output.ToString());
            }

            [Command("clan members")]
            [Summary("Lists all members of the given clan in the database")]
            public async Task ListClanMemberAsync([Summary("The id of the clan")]long clanId)
            {
                var clan = await Database.Clans.FirstOrDefaultAsync(c => c.Id == clanId);
                StringBuilder output = new StringBuilder();
                foreach (var member in clan.Members)
                {
                    output.AppendLine($"{member.Id}: {member.Name}");
                }
                await ReplyAsync(output.ToString());
            }

            [Command("members")]
            [Summary("List all members of every clan in the database")]
            public async Task ListMembersAsync()
            {
                var members = await Database.Members.ToListAsync();
                StringBuilder output = new StringBuilder();
                foreach (var member in members)
                {
                    output.AppendLine($"{member.Id}: {member.Name} {(member.Role == ClanRole.Admin? "[Admin]":"")} - {member.ClanId}");
                }
                await ReplyAsync(output.ToString());
            }
        }

        [Group("delete")]
        public class Purge : ModuleBase<SocketCommandContext>
        {
            public Purge(DonationDatabase database)
            {
                Database = database;
            }

            public DonationDatabase Database { get; }

            [Command("clan")]
            [Summary("Deletes the specified clan, the clan must be empty")]
            public async Task PurgeClanAsync([Summary("The id of the clan")]long clanId)
            {
                var clan = await Database.Clans.FirstOrDefaultAsync(c => c.Id == clanId);
                if (clan != null)
                {
                    Database.Remove(clan);
                    await Database.SaveChangesAsync();
                    await ReplyAsync("Clan deleted");
                }
                else
                {
                    await ReplyAsync("Clan not found");
                }
            }

            [Command("member")]
            [Summary("Deletes the specified member")]
            public async Task DeleteClanMemberAsync([Summary("The id of the member")]long memberId)
            {
                var member = await Database.Members.FirstOrDefaultAsync(m => m.Id == memberId);
                if (member != null)
                {
                    Database.Remove(member);
                    await Database.SaveChangesAsync();
                    await ReplyAsync("Member deleted");
                }
                else
                {
                    await ReplyAsync("Member not found");
                }
            }
        }

        [Group("update")]
        public class Update : ModuleBase<SocketCommandContext>
        {
            public Update(DonationDatabase database)
            {
                Database = database;
            }
            public DonationDatabase Database { get; }

            [Command("role")]
            [Summary("Sets the role of the given member")]
            public async Task UpdateRoleAsync([Summary("The id of the member")]long memberId, [Summary("The role, must be Member or Admin")]ClanRole role)
            {
                var member = await Database.Members.FirstOrDefaultAsync(m => m.Id == memberId);
                if (member != null)
                {
                    member.Role = role;
                    Database.Update(member);
                    await Database.SaveChangesAsync();
                    await ReplyAsync("Role updated");
                }
                else
                {
                    await ReplyAsync("Member not found");
                }
            }
        }
    }
}

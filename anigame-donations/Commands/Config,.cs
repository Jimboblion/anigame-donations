using Jimboblion.Anigame.Donations;
using Discord;
using Discord.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace Jimboblion.Anigame.Donations.Commands
{
    public class ConfigModule : ModuleBase<SocketCommandContext>
    {
        private readonly DonationDatabase database;
        private readonly Config defaultConfig;

        public ConfigModule(DonationDatabase database, Config defaultConfig)
        {
            this.database = database;
            this.defaultConfig = defaultConfig;
        }

        [Command("prefix")]
        public async Task GetOrSetPrefixAsynx(string newPrefix = "")
        {
            if (string.IsNullOrWhiteSpace(newPrefix))
            {
                var guild = await database.Guilds.FindAsync((long)(Context.Guild.Id));
                if (guild != null)
                    await ReplyAsync($"Prefix is {guild.Prefix}");
                else
                    await ReplyAsync($"Prefix is {defaultConfig.Prefix}");
            }
            else
            {
                var guild = await database.Guilds.FindAsync((long)(Context.Guild.Id));
                if (guild == null)
                {
                    guild = new Guild() 
                    {
                        Prefix = newPrefix,
                        Id = (long)Context.Guild.Id,
                        Name = Context.Guild.Name
                    };
                    await database.Guilds.AddAsync(guild);                    
                }
                else 
                {
                    guild.Prefix = newPrefix;
                    database.Guilds.Update(guild);
                }
                await database.SaveChangesAsync();
                await ReplyAsync($"Prefix set to {newPrefix}");
            }
        }
    }
}

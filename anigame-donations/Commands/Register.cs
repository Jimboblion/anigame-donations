﻿using Discord.Commands;
using Jimboblion.Anigame.Donations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jimboblion.Anigame.Donations.Commands
{
    [Group("register")]
    public class RegisterModule : ModuleBase<SocketCommandContext>
    {
        private readonly DonationDatabase database;

        public RegisterModule(DonationDatabase database)
        {
            this.database = database;
        }

        [Command("clan")]
        [Summary("Registers a clan with the donations bot")]
        public async Task AddClanAsync([Summary("The id of the clan")]long clanId, [Summary("The name of the clan")][Remainder] string name)
        {
            var clan = await database.Clans.FindAsync(clanId);
            if (clan != null)
            {
                long author = (long)Context.Message.Author.Id;
                var player = await database.Members.FindAsync(author);
                if (player == null || player.ClanId != clan.Id)
                {
                    await ReplyAsync("Clan is already registered");
                }
                else
                {
                    clan.Name = name;
                    database.Clans.Update(clan);
                    await database.SaveChangesAsync();
                    await ReplyAsync($"Clan name updated to {name}");
                }
            }
            else
            {
                long author = (long)Context.Message.Author.Id;
                var player = await database.Members.FindAsync(author);

                clan = new Clan()
                {
                    Id = clanId,
                    Name = name
                };

                if (player != null)
                {
                    if (player.ClanId != null)
                    {
                        var playerClan = await database.Clans.FindAsync(player.ClanId.Value);
                        await ReplyAsync($"You are already a member of {playerClan.Name}");
                        return;
                    }
                    else
                    {
                        player.ClanId = clanId;
                        player.Role = ClanRole.Admin;
                        database.Members.Update(player);
                    }
                }
                else
                {
                    var member = new Member()
                    {
                        Id = (long)Context.Message.Author.Id,
                        Name = Context.Message.Author.Username,
                        JoinedOn = DateTime.UtcNow,
                        Role = ClanRole.Admin,
                        ClanId = clanId
                    };
                    database.Members.Add(member);
                }

                database.Clans.Add(clan);
                await database.SaveChangesAsync();
                await ReplyAsync($"Clan added, members: 1");
            }
        }

        [Command("member")]
        [Summary("Registers a member with your clan")]
        public async Task AddMemberAsync([Summary("The id of the member")] string memberId)
        {
            try
            {
                if (!memberId.StartsWith("<@"))
                {
                    await ReplyAsync("Incorrect format, format is `.register @member`");
                    return;
                }
                memberId = memberId.Trim('>', '<', '!', '@');
                var member = await database.Members.FindAsync(long.Parse(memberId));
                if (member == null)
                {
                    var user = Context.Guild.GetUser(ulong.Parse(memberId));
                    var memberName = user?.Username ?? null;
                    member = new Member()
                    {
                        Id = long.Parse(memberId),
                        Name = memberName,
                    };
                    database.Members.Add(member);
                    await database.SaveChangesAsync();
                }
                var author = (long)Context.Message.Author.Id;
                var existingMember = await database.Members.FindAsync(author);
                if (existingMember == null)
                {
                    await ReplyAsync("You are not a member of a clan, only clan admins can add new members");
                    return;
                }
                if (existingMember.Role != ClanRole.Admin)
                {
                    await ReplyAsync("You are not an admin of a clan, only clan admins can add new members");
                    return;
                }
                var clanId = existingMember.ClanId;
                var clan = await database.Clans.FindAsync(clanId);
                if (clan != null)
                {
                    if (member.ClanId != clanId)
                    {
                        member.ClanId = clanId;
                        member.JoinedOn = DateTime.UtcNow;
                        member.DonationStart = DateTime.UtcNow;
                        member.BalanceAtStart = 0;
                        member.Role = ClanRole.Member;
                        database.Members.Update(member);
                        await database.SaveChangesAsync();
                        var memberCount = await database.Members.CountAsync(m => m.ClanId == clanId);
                        await ReplyAsync($"Member added to {clan.Name}, members: {memberCount}");
                    }
                    else
                    {
                        await ReplyAsync("Member is already part of this clan");
                    }
                }
                else
                {
                    await ReplyAsync("Clan not found");
                }
            }
            catch (Exception ex)
            {
                var message = "Unable to register user: " + ex.Message + "\r\n\r\n" + ex.StackTrace;
                await ReplyAsync(message.Remove(1000));
            }
        }
    }
}

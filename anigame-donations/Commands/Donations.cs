﻿using Discord;
using Discord.Commands;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jimboblion.Anigame.Donations.Commands
{
    public class DonationsModule : ModuleBase<SocketCommandContext>    
    {
        private readonly DonationDatabase database;

        public DonationsModule(DonationDatabase database)
        {
            this.database = database;
        }

        [Command("balance"), Alias("b")]
        [Summary("Gets your current balance")]
        public async Task PlayerDonationsAsync()
        {
            var author = (long)Context.Message.Author.Id;
            var m = await database.Members.AsQueryable().Where(m => m.Id == author).Select(m => new Member
            {
                Id = m.Id,
                Name = m.Name,
                BalanceAtStart = m.BalanceAtStart,
                DonationStart = m.DonationStart,
                ExpectedDonation = m.ExpectedDonation,
                Donations = m.Donations.Where(d => d.Time > m.DonationStart)
            }).FirstOrDefaultAsync();
            if (m != null) 
            {
                var balance = CalculateBalance(m);
                var timeLeft = CalculateTime(balance, m.ExpectedDonation);
                await ReplyAsync($"Current Balance: {balance:N0}/{m.ExpectedDonation} ({timeLeft.TotalDays:N1} days)");
            }
            else 
            {
                await ReplyAsync("You are not registered with anigame donations");
            }
        }

        [Command("balance clan")]
        [Summary("Shows the balance of all members in your clan")]
        public async Task ClanDonationsAsync()
        {
            var author = (long)Context.Message.Author.Id;
            var member = await database.Members.FindAsync(author);
            var clanId = member.ClanId;
            var donations = await database.Members.AsQueryable().Where(m => m.ClanId == clanId).Select(m => new Member
            {
                Id = m.Id,
                Name = m.Name,
                BalanceAtStart = m.BalanceAtStart,
                DonationStart = m.DonationStart,
                ExpectedDonation = m.ExpectedDonation,
                Donations = m.Donations.Where(d => d.Time > m.DonationStart)
            }).ToListAsync();

            var currentBalances = donations.Select(m => new
            {
                Member = m,
                Balance = CalculateBalance(m)
            }).OrderBy(b => b.Balance);


            var builder = new EmbedBuilder();
            builder.Author = new EmbedAuthorBuilder();
            builder.Author.Name = Context.Client.CurrentUser.Username;
            builder.Author.IconUrl = Context.Client.CurrentUser.GetAvatarUrl();
            builder.Title = "Clan Balances";
            builder.Description = string.Join("\r\n", currentBalances.Select(b => $"{b.Member.Name??b.Member.Id.ToString()}: {b.Balance:N0}/{b.Member.ExpectedDonation} ({CalculateTime(b.Balance, b.Member.ExpectedDonation).TotalDays:N1} days)"));
            await ReplyAsync(embed: builder.Build());
        }

        private static double CalculateBalance(Member m)
        {
            return m.BalanceAtStart +
                m.Donations.Sum(d => d.Amount)
                - (m.ExpectedDonation / 7) * (DateTime.UtcNow - m.DonationStart).TotalDays;
        }

        private TimeSpan CalculateTime(double balance, long expectedDonation)
        {
            if (expectedDonation == 0)
            {
                return TimeSpan.Zero;
            }
            return TimeSpan.FromDays(balance/(expectedDonation/7.0));
        }

        [Command("expect")]
        [Summary("Sets the amount of gold expected for a 7 day period")]
        public async Task SetExpectationAysnc([Summary("The id of the member donating")]string memberId, [Summary("The amount they are expected to donate")]long amount)
        {
            try
            {
                var authorId = (long)Context.Message.Author.Id;
                var author = await database.Members.FindAsync(authorId);
                if (author == null)
                {
                    await ReplyAsync("You are not in a clan");
                    return;
                }
                if (!memberId.StartsWith("<@"))
                {
                    await ReplyAsync($"`expect {memberId} {amount}` is in the incorrect format, format is `.expect @member amount`");
                    return;
                }
                memberId = memberId.Trim('>', '<', '!','@');
                var member = await database.Members.FindAsync(long.Parse(memberId));
                if (member == null || member.ClanId != author.ClanId)
                {
                    await ReplyAsync($"{member.Name} is not in your clan");
                    return;
                }
                if (author.Role != ClanRole.Admin)
                {
                    await ReplyAsync("Only clan admins can set member expectations");
                    return;
                }
                var memberWithDonations = await database.Members.AsQueryable().Select(m => new Member
                {
                    Id = m.Id,
                    Name = m.Name,
                    BalanceAtStart = m.BalanceAtStart,
                    DonationStart = m.DonationStart,
                    ExpectedDonation = m.ExpectedDonation,
                    Donations = m.Donations.Where(d => d.Time > m.DonationStart)
                }).Where(m => m.Id == long.Parse(memberId)).FirstAsync();

                var currentBalance = CalculateBalance(memberWithDonations);
                member.BalanceAtStart = (long)currentBalance;
                member.DonationStart = DateTime.UtcNow;
                member.ExpectedDonation = amount;
                database.Members.Update(member);
                await database.SaveChangesAsync();
                await ReplyAsync($"Expectation set to {amount} per week");
            }
            catch(Exception ex)
            {
                await ReplyAsync("Unable to set expectation: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
            }
        }

        [Command("fine")]
        [Summary("Adds a fine to the specified member")]
        public async Task RecordDonationAsync([Summary("The member to fine")]string memberId, [Summary("The amount to fine them")]long amount)
        {
            var authorId = (long)Context.Message.Author.Id;
            var author = await database.Members.FindAsync(authorId);
            if (author == null)
            {
                await ReplyAsync("You are not in a clan");
                return;
            }
            if (!memberId.StartsWith("<@"))
            {
                await ReplyAsync($"`fine {memberId} {amount}` is in the incorrect format, format is `.expect @member amount`");
                return;
            }
            memberId = memberId.Trim('>', '<', '!', '@');
            var member = await database.Members.FindAsync(long.Parse(memberId));
            if (member == null || member.ClanId != author.ClanId)
            {
                await ReplyAsync($"{member.Name} is not in your clan");
                return;
            }
            if (author.Role != ClanRole.Admin)
            {
                await ReplyAsync("Only clan admins can fine members");
                return;
            }
            if (member != null && member.ClanId != null)
            {
                await database.Donations.AddAsync(new Donation() { ClanId = member.ClanId.Value, MemberId = member.Id, Time = DateTime.UtcNow, Amount = -amount });
                if (string.IsNullOrEmpty(member.Name))
                {
                    member.Name = Context.Message.Author.Username;
                    database.Update(member);
                }
                await database.SaveChangesAsync();
                await ReplyAsync("Fine Recorded");
            }
        }
    }
}

﻿using Discord.Commands;
using Jimboblion.Anigame.Donations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Jimboblion.Anigame.Donations.Commands
{
    [Group("remove")]
    public class RemoveModule : ModuleBase<SocketCommandContext>
    {
        private readonly DonationDatabase database;

        public RemoveModule(DonationDatabase database)
        {
            this.database = database;
        }

        [Command("clan")]
        [Summary("Removes a clan from the donations bot")]
        public async Task RemoveClanAsync([Summary("The id of the clan")] long clanId)
        {
            var author = (long)Context.Message.Author.Id;
            var member = await database.Members.FindAsync(author);
            var clan = await database.Clans.FindAsync(clanId);
            if (clan != null)
            {
                if (member.ClanId == clan.Id && member.Role == ClanRole.Admin)
                {
                    await DeleteClan(clan);
                    await ReplyAsync($"Clan {clan.Name} Removed");
                }
                else
                {
                    await ReplyAsync("Only clan admins of a clan can remove it");
                }
            }
            else
            {
                await ReplyAsync($"Clan not found");
            }
        }

        private async Task DeleteClan(Clan clan)
        {
            await database.Database.ExecuteSqlRawAsync($"DELETE FROM \"{database.Donations.EntityType.GetTableName()}\" " +
                $"WHERE \"{database.Donations.FieldName(d => d.ClanId)}\" = @p0", clan.Id);
            foreach (var clanMember in await database.Members.AsQueryable().Where(m => m.ClanId == clan.Id).ToListAsync())
            {
                clanMember.ClanId = null;
                clanMember.Role = ClanRole.None;
                database.Members.Update(clanMember);
            }
            database.Clans.Remove(clan);
            await database.SaveChangesAsync();
        }

        [Command("member")]
        [Summary("Removes a member from your clan")]
        public async Task RemoveMemberAsync([Summary("The id of the member")] string memberId)
        {
            if (!memberId.StartsWith("<@"))
            {
                await ReplyAsync("Incorrect format, format is `.register @member clanid`");
                return;
            }
            memberId = memberId.Trim('>', '<', '!', '@');
            var member = await database.Members.FindAsync(long.Parse(memberId));
            if (member != null)
            {
                if (member.ClanId != null)
                {
                    var clan = member.Clan;
                    member.ClanId = null;
                    member.Role = ClanRole.None;
                    database.Members.Update(member);
                    if (clan.Members.Count == 1)
                    {
                        await DeleteClan(clan);
                        await ReplyAsync("Last member removed, clan deleted");
                    }
                    else
                    {
                        await database.SaveChangesAsync();
                        await ReplyAsync($"Member removed from clan");
                    }
                }
                else
                {
                    await ReplyAsync("Member is not part of clan");
                }
            }
            else
            {
                await ReplyAsync("Member is not registered");
            }
        }
    }
}

﻿using Discord;
using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jimboblion.Anigame.Donations.Commands
{

    public class Help : ModuleBase<SocketCommandContext>
    {
        CommandService commandService;
        private readonly IUtility utility;
        IServiceProvider services;
        public Help(CommandService service, IUtility utility, IServiceProvider services)
        {
            commandService = service;
            this.utility = utility;
            this.services = services;
        }
        [Command("help")]
        [Summary("Displays a list of available Anigame donations commands [and aliases].")]
        public async Task ListCommandsAsync()
        {
            var prefix = await utility.Prefix(Context.Guild);
            var commands = new List<string>();

            foreach (var command in commandService.Commands)
            {
                var result = await command.CheckPreconditionsAsync(Context, services);
                if (result.IsSuccess)
                {
                    var line = new StringBuilder();
                    var commandName = FullCommand(command);
                    line.Append($"`{prefix}{commandName}` ");
                    if (command.Aliases.Any(a => a != commandName))
                    {
                        line.Append("[");
                        foreach (var alias in command.Aliases.Where(a => a != commandName))
                            line.Append($"`{prefix}{alias}` ");
                        line.Append("]");
                    };
                    commands.Add(line.ToString());
                }
            }

            var builder = new EmbedBuilder();
            builder.Author = new EmbedAuthorBuilder();
            builder.Author.Name = Context.Client.CurrentUser.Username;
            builder.Author.IconUrl = Context.Client.CurrentUser.GetAvatarUrl();
            builder.Title = "Mighty Bot Commands";
            builder.Description = $"Use `{prefix}help <command>` for more information about each command.";
            builder.Fields = new List<EmbedFieldBuilder>();
            builder.Fields.Add(new EmbedFieldBuilder().WithName($"Commands").WithValue(string.Join("\r\n", commands.OrderBy(c => c))).WithIsInline(true));
            await ReplyAsync(embed: builder.Build());
        }

        [Command("help")]
        [Alias("command")]
        [Summary("Displays help about the specified command.\r\nFor a list of commands, see `$prefix$commands`.\r\n" +
                 "● - required parameter\r\n" +
                 "? - optional parameter\r\n" +
                 "[text] - text to clarify some aspect of a parameter in documentation, not part of the actual command\r\n" +
                 "Parameter,aliases <*value*> - command keywords\r\n" +
                 "<*parameter/value:list/of/valid/values*> - list of valid values for the given parameter\r\n" +
                 "__underlined value__ - default parameter value")]
        public async Task ShowCommandAsync([Remainder] string commandName)
        {
            var allCommands = new StringBuilder();
            var command = commandService.Commands.FirstOrDefault(c => c.Name.ToLower() == commandName.ToLower());
            if (command == null)
                command = commandService.Commands.FirstOrDefault(c => c.Aliases.Any(a => a.ToLower() == commandName.ToLower()));
            if (command != null)
            {
                var commandParameters = new StringBuilder();

                if (command.Parameters != null)
                {
                    foreach (var parameter in command.Parameters)
                    {
                        commandParameters.AppendLine(ParameterHelp(parameter.Name, parameter.Summary, parameter.IsOptional, false, "", ""));
                    }
                }

                var builder = new EmbedBuilder();
                var prefix = await utility.Prefix(Context.Guild);
                builder.Author = new EmbedAuthorBuilder();
                builder.Author.Name = Context.Client.CurrentUser.Username;
                builder.Author.IconUrl = Context.Client.CurrentUser.GetAvatarUrl();
                var name = FullCommand(command);
                builder.Title = $"Command {name}";
                builder.Fields = new List<EmbedFieldBuilder>();
                if (command.Summary != null)
                    builder.Description = command.Summary.Replace("$prefix$", prefix);
                builder.Fields.Add(new EmbedFieldBuilder().WithName("Syntax").WithValue(BuildSyntax(prefix, command)));
                if (command.Aliases.Any(a => a != name))
                    builder.Fields.Add(new EmbedFieldBuilder().WithName("Aliases").WithValue(string.Join(',', command.Aliases.Where(a => a != name))));
                if (commandParameters.Length > 0)
                    builder.Fields.Add(new EmbedFieldBuilder().WithName("Parameters").WithValue(commandParameters.ToString().Replace("$prefix$", prefix)));
                await ReplyAsync(embed: builder.Build());
            }
        }

        private static string ParameterHelp(string name, string summary, bool optional, bool named, string aliases, string validvalues)
        {
            string parameterHelp = "";
            if (optional)
                parameterHelp += "? ";
            else
                parameterHelp += "● ";

            if (named)
            {
                if (!String.IsNullOrWhiteSpace(aliases))
                    parameterHelp += $"{name},{aliases} ";
                else
                    parameterHelp += $"{name} ";

                if (!String.IsNullOrWhiteSpace(validvalues))
                {
                    if (validvalues != "none")
                        parameterHelp += $"<*value:{validvalues}*>";
                }
                else
                    parameterHelp += "<*value*>";
            }
            else
            {
                if (!String.IsNullOrWhiteSpace(validvalues))
                {
                    if (validvalues != "none")
                        parameterHelp += $"<*{name}:{validvalues}*>";
                    else
                        parameterHelp += $"<*{name}*>";
                }
                else
                    parameterHelp += $"<*{name}*>";
            }
            parameterHelp += $" - {summary}";
            return parameterHelp;
        }

        private static string BuildSyntax(string prefix, CommandInfo command)
        {
            var name = FullCommand(command);
            string syntax = $"{prefix}{name}";
            foreach (var parameter in command.Parameters)
            {
                syntax += $" <{parameter.Name}>";
            }
            return $"`{syntax}`";
        }

        private static string FullCommand(CommandInfo command)
        {
            var module = command.Module;
            var name = command.Name;
            while (module != null)
            {
                if (!string.IsNullOrEmpty(module.Group))
                    name = $"{module.Group} {name}";
                module = module.Parent;
            }
            return name;
        }
    }
}

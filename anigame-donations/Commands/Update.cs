﻿using Discord;
using Discord.Commands;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jimboblion.Anigame.Donations.Commands
{
    [Group("set")]
    public class UpdateModule : ModuleBase<SocketCommandContext>
    {
        private readonly DonationDatabase database;

        public UpdateModule(DonationDatabase database)
        {
            this.database = database;
        }

        [Command("memberName")]
        [Summary("Updates the name of the member, used in the `balance clan` command")]
        public async Task SetMemberNameAsync([Summary("The id of the member")] string memberId, [Summary("The name of the member")] string name)
        {
            if (!memberId.StartsWith("<@"))
            {
                await ReplyAsync("Incorrect format, format is `.set memberName @member`");
                return;
            }            
            memberId = memberId.Trim('>', '<', '!', '@');
            var member = await database.Members.FindAsync(long.Parse(memberId));
            if (member != null)
            {
                long authorId = (long)Context.Message.Author.Id;
                var author = await database.Members.FindAsync(authorId);
                if (author.Role != ClanRole.Admin)
                {
                    await ReplyAsync("Only clan admins can set member names");
                    return;
                }
                if (member.ClanId == null || member.ClanId != author.ClanId)
                {
                    await ReplyAsync("Member isn't part of your clan");
                    return;
                }
                member.Name = name;
                database.Update(member);
                await database.SaveChangesAsync();
                await ReplyAsync("Member Name Updated");
            }
            else
            {
                await ReplyAsync("Member not found");
            }
        }

        [Command("balance")]
        [Summary("Sets the members balance")]
        public async Task SetMemberBalanceAsync([Summary("The id of the member")] string memberId, [Summary("The new balance")] long balance)
        {
            if (!memberId.StartsWith("<@"))
            {
                await ReplyAsync("Incorrect format, format is `.set balance @member`");
                return;
            }
            memberId = memberId.Trim('>', '<', '!', '@');
            var member = await database.Members.FindAsync(long.Parse(memberId));
            if (member != null)
            {
                long authorId = (long)Context.Message.Author.Id;
                var author = await database.Members.FindAsync(authorId);
                if (author.Role != ClanRole.Admin)
                {
                    await ReplyAsync("Only clan admins can set member balances");
                    return;
                }
                if (member.ClanId == null || member.ClanId != author.ClanId)
                {
                    await ReplyAsync("Member isn't part of your clan");
                    return;
                }
                member.BalanceAtStart = balance;
                member.DonationStart = DateTime.UtcNow;
                database.Update(member);
                await database.SaveChangesAsync();
                await ReplyAsync("Member Name Updated");
            }
            else
            {
                await ReplyAsync("Member not found");
            }
        }
    }
}

﻿using Jimboblion.Anigame.Donations;
using Discord;
using Discord.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Jimboblion.Anigame.Donations.Commands
{
    [Group("cl"), Alias("clan")]
    public class ClanModule : ModuleBase<SocketCommandContext>
    {
        private readonly DonationDatabase database;
        private readonly IDonationCache cache;

        public ClanModule(ILogger<ClanModule> logger, DonationDatabase database, IDonationCache cache)
        {
            Logger = logger;
            this.database = database;
            this.cache = cache;
        }

        public ILogger<ClanModule> Logger { get; }

        [Command("donate")]
        [Summary("Records an anigame donation, only works if the prefix of the donations bot matches the prefix of the anigame bot and both can see the channel used")]
        public async Task RecordDonationAsync([Summary("The amount donated")] long amount)
        {
            try
            {
                var name = Context.Message.Author.Username;
                if (name.Contains('#'))
                    name = name.Remove(name.IndexOf('#'));
                await cache.AddPending(new PendingDonation(Context.Message.Author.Id, name, amount, DateTime.UtcNow.Ticks));
                foreach (var cacheEntry in cache.Entries)
                {
                    if (cacheEntry.Username == name && amount == cacheEntry.Amount)
                    {
                        await cache.Remove(cacheEntry);
                        //How to make await?
                        await AddDonationAsync(amount);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Error reocrding donation");
            }
        }

        private async Task AddDonationAsync(long amount)
        {
            //Who donated?
            long author = (long)Context.Message.Author.Id;
            //which clan?
            var member = await database.Members.FindAsync(author);
            if (member != null && member.ClanId != null)
            {
                await database.Donations.AddAsync(new Donation() { ClanId = member.ClanId.Value, MemberId = member.Id, Time = DateTime.UtcNow, Amount = amount });
                if (string.IsNullOrEmpty(member.Name))
                {
                    member.Name = Context.Message.Author.Username;
                    database.Update(member);
                }
                await database.SaveChangesAsync();
                await ReplyAsync("Donation Recorded");
            }
        }
    }
}

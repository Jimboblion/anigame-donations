﻿using Discord;
using Discord.Commands;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jimboblion.Anigame.Donations.Commands
{
    public class RolesModule : ModuleBase<SocketCommandContext>
    {
        private readonly DonationDatabase database;

        public RolesModule(DonationDatabase database)
        {
            this.database = database;
        }

        [Command("promote")]
        [Summary("Promotes a member to be an admin")]
        public async Task PromoteMemberAsync([Summary("The id of the member")] string memberId)
        {
            if (!memberId.StartsWith("<@"))
            {
                await ReplyAsync("Incorrect format, format is `.promote @member`");
                return;
            }            
            memberId = memberId.Trim('>', '<', '!', '@');
            var member = await database.Members.FindAsync(long.Parse(memberId));
            if (member != null)
            {
                long authorId = (long)Context.Message.Author.Id;
                var author = await database.Members.FindAsync(authorId);
                if (author.Role != ClanRole.Admin)
                {
                    await ReplyAsync("Only clan admins can promote members");
                    return;
                }
                if (member.ClanId == null || member.ClanId != author.ClanId)
                {
                    await ReplyAsync("Member isn't part of your clan");
                    return;
                }
                member.Role = ClanRole.Admin;
                database.Update(member);
                await database.SaveChangesAsync();
                await ReplyAsync("Member Promoted to Admin");
            }
            else
            {
                await ReplyAsync("Member not found");
            }
        }

        [Command("demote")]
        [Summary("Demotes an admin to be a member")]
        public async Task DemoteMemberAsync([Summary("The id of the admin")] string memberId)
        {
            if (!memberId.StartsWith("<@"))
            {
                await ReplyAsync("Incorrect format, format is `.demote @member`");
                return;
            }
            memberId = memberId.Trim('>', '<', '!', '@');
            var member = await database.Members.FindAsync(long.Parse(memberId));
            if (member != null)
            {
                long authorId = (long)Context.Message.Author.Id;
                var author = await database.Members.FindAsync(authorId);
                if (author.Role != ClanRole.Admin)
                {
                    await ReplyAsync("Only clan admins can demote members");
                    return;
                }
                if (member.ClanId == null || member.ClanId != author.ClanId)
                {
                    await ReplyAsync("Member isn't part of your clan");
                    return;
                }
                member.Role = ClanRole.Member;
                database.Update(member);
                await database.SaveChangesAsync();
                await ReplyAsync("Admin Demoted to Member");
            }
            else
            {
                await ReplyAsync("Member not found");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jimboblion.Anigame.Donations
{
    public class Member
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime JoinedOn { get; set; }
        public long? ClanId { get; set; }
        public Clan Clan { get; set; }
        public ClanRole Role { get; set; }
        public int Level { get; set; }
        public long ExpectedDonation { get; set; }
        public DateTime DonationStart { get; set; }
        public long BalanceAtStart { get; set; }
        public IEnumerable<Donation> Donations { get; set; } = new List<Donation>();
    }
}
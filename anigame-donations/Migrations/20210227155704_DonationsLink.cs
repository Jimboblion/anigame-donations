﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Jimboblion.Anigame.Donations.Migrations
{
    public partial class DonationsLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "BalanceAtStart",
                table: "Members",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "DonationStart",
                table: "Members",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateIndex(
                name: "IX_Donations_MemberId",
                table: "Donations",
                column: "MemberId");

            migrationBuilder.AddForeignKey(
                name: "FK_Donations_Members_MemberId",
                table: "Donations",
                column: "MemberId",
                principalTable: "Members",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Donations_Members_MemberId",
                table: "Donations");

            migrationBuilder.DropIndex(
                name: "IX_Donations_MemberId",
                table: "Donations");

            migrationBuilder.DropColumn(
                name: "BalanceAtStart",
                table: "Members");

            migrationBuilder.DropColumn(
                name: "DonationStart",
                table: "Members");
        }
    }
}

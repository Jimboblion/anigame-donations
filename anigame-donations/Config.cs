﻿namespace Jimboblion.Anigame.Donations
{
    public class Config
    {
        public string Token { get; set; }
        public string Prefix { get; set; } = ".";
    }
}
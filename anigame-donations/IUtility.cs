﻿using Discord.WebSocket;
using System.Threading.Tasks;

namespace Jimboblion.Anigame.Donations
{
    public interface IUtility
    {
        Task<string> Prefix(SocketGuild guildId);
    }
}
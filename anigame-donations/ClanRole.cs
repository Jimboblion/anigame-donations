﻿namespace Jimboblion.Anigame.Donations
{
    public enum ClanRole
    {
        None,
        Member,
        Admin
    }
}
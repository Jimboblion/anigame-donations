﻿using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jimboblion.Anigame.Donations
{
    class DonationCache : IDonationCache
    {
        //response from bot read before message from user
        private List<BotDonation> cache = new List<BotDonation>();
        //Message from user read before response from bot
        private List<PendingDonation> pendingCache = new List<PendingDonation>();
        private readonly DonationDatabase database;

        public IEnumerable<BotDonation> Entries => cache;

        public ILogger<DonationCache> Logger { get; }

        public DonationCache(DonationDatabase database, ILogger<DonationCache> logger)
        {
            this.database = database;
            Logger = logger;
        }

        //Record time, username and amount
        //event NewDonation
        //Message from bot
        public async Task Add(SocketUserMessage message)
        {
            //Summoner **{name}**, you have donated __{gold}__ Gold <:GOLD: 704242802522980462> to your clan, and received __{rubies}__ Clan Rubies<:CLAN_RUBIES:713100255536742450> in return !
            var time = message.Timestamp;
            var embed = message.Embeds.First();
            var parts = embed.Description.Split("**");
            var name = parts[1];
            var amount = int.Parse(parts[3].Replace(",",""));
            bool pendingFound = false;
            //Now scan pending
            foreach (var pending in pendingCache)
            {
                if (name == pending.Name && amount == pending.Amount)
                {
                    pendingCache.Remove(pending);
                    //How to make await?
                    pendingFound = true;
                    await AddDonationAsync(message.Channel, pending.AuthorId, amount);
                    break;
                }
            }
            if (!pendingFound)
                cache.Add(new BotDonation(name, amount, time));
        }

        private async Task AddDonationAsync(ISocketMessageChannel channel, ulong author, long amount)
        {
            //which clan?
            var member = await database.Members.FirstOrDefaultAsync(x => x.Id == (long)author);
            if (member != null && member.ClanId != null)
            {
                await database.Donations.AddAsync(new Donation() { ClanId = member.ClanId.Value, MemberId = member.Id, Time = DateTime.UtcNow, Amount = amount });
                await database.SaveChangesAsync();
                await channel.SendMessageAsync("Donation Recorded");
            }
        }

        public async Task AddPending(PendingDonation pendingDonation)
        {
            pendingCache.RemoveAll(p => p.Ticks < DateTime.UtcNow.Subtract(TimeSpan.FromMinutes(1)).Ticks);
            pendingCache.Add(pendingDonation);
        }

        public bool IsDonation(SocketUserMessage message)
        {
            Logger.LogInformation(message.Content);
            if (message.Embeds.Any())
            {
                Logger.LogInformation($"Embeds: {message.Embeds.Count}");
                var embedDescription = message.Embeds.First().Description;
                if (!string.IsNullOrWhiteSpace(embedDescription))
                {
                    return embedDescription.Contains("you have donated") && embedDescription.Contains("to your clan");
                }
                else 
                {
                    Logger.LogInformation($"Unable to read embed: {message.Timestamp} - {message.Embeds.First().Title}");
                }
            }
            return false;
        }

        public async Task Remove(BotDonation cacheEntry)
        {
            cache.Remove(cacheEntry);
        }
    }

    public class PendingDonation
    {
        public PendingDonation(ulong authorId, string name, long amount, long ticks)
        {
            AuthorId = authorId;
            Name = name;
            Amount = amount;
            Ticks = ticks;
        }

        public ulong AuthorId { get; }
        public string Name { get; }
        public long Amount { get; }
        public long Ticks { get; }
    }

    public class BotDonation
    {
        public BotDonation(string name, int amount, DateTimeOffset time)
        {
            Username = name;
            Amount = amount;
            Time = time;
        }

        public string Username { get; }
        public int Amount { get; }
        public DateTimeOffset Time { get; }
    }
}

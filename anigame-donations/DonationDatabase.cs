﻿using Microsoft.EntityFrameworkCore;

namespace Jimboblion.Anigame.Donations
{
    public class DonationDatabase : DbContext
    {
        private readonly string connectionString;

        public DonationDatabase()
        {
            connectionString = System.Environment.GetEnvironmentVariable("ConnectionString__A");
        }

        public DonationDatabase(string connectionString) : base()
        {
            this.connectionString = connectionString;
        }
        public DonationDatabase(DbContextOptions<DonationDatabase> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseNpgsql(connectionString);
        }

        public DbSet<Clan> Clans { get; internal set; }
        public DbSet<Donation> Donations { get; internal set; }
        public DbSet<Member> Members { get; internal set; }
        public DbSet<Guild> Guilds { get; internal set; }
    }
}
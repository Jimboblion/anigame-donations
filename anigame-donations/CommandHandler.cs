﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Diagnostics;
using System.Threading;
using Microsoft.EntityFrameworkCore;

namespace Jimboblion.Anigame.Donations
{
    internal class CommandHandler
    {
        private readonly DiscordShardedClient _client;
        private readonly CommandService _commands;
        private readonly Config _config;
        private IServiceProvider services;
        private bool ready = false;

        // Retrieve client and CommandService instance via ctor
        public CommandHandler(DiscordShardedClient client, Config config)
        {
            _commands = new CommandService();
            _client = client;
            _config = config;
        }

        public async Task InstallCommandsAsync()
        {
            // Hook the MessageReceived event into our command handler
            _client.MessageReceived += HandleCommandAsync;
            var connectionString = System.Environment.GetEnvironmentVariable("ConnectionString__A");
            var collection = new ServiceCollection();
            collection.AddSingleton<IDiscordClient>(_client);
            collection.AddScoped<Config>(provider => _config);
            collection.AddScoped<IUtility, Utility>();
            collection.AddScoped<IDonationCache, DonationCache>();
            collection.AddDbContext<DonationDatabase>(options => options.UseNpgsql(connectionString).EnableSensitiveDataLogging());
            collection.AddLogging(c => c.AddConsole());
            var builder = new DefaultServiceProviderFactory();
            services = builder.CreateServiceProvider(collection);

            var database = services.GetService<DonationDatabase>();
            database.Database.Migrate();
            // Here we discover all of the command modules in the entry 
            // assembly and load them. Starting from Discord.NET 2.0, a
            // service provider is required to be passed into the
            // module registration method to inject the 
            // required dependencies.
            //
            // If you do not use Dependency Injection, pass null.
            // See Dependency Injection guide for more information.
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), services);
            ready = true;
        }

        private async Task HandleCommandAsync(SocketMessage messageParam)
        {
            if (!ready)
                return;
            var commandTimer = Stopwatch.StartNew();
            // Don't process the command if it was a system message
            var message = messageParam as SocketUserMessage;
            if (message == null) return;

            // Create a number to track where the prefix ends and the command begins
            int argPos = 0;
            // Create a WebSocket-based command context based on the message
            var context = new ShardedCommandContext(_client, message);

            var utility = services.GetService<IUtility>();

            var prefix = await utility.Prefix(context.Guild);
            var logger = services.GetService<ILogger<CommandHandler>>();

            try
            {
                if (message.Author.IsBot && message.Author.Id == 571027211407196161)
                {
                    var donationCache = services.GetService<IDonationCache>();
                    if (donationCache.IsDonation(message))
                        await donationCache.Add(message);
                }
            }
            catch (Exception ex)
            {
                logger.LogInformation(ex, "Error reading anigame bot message");
            }
            // Determine if the message is a command based on the prefix and make sure no bots trigger commands
            if (!(message.HasStringPrefix(prefix, ref argPos) ||
                message.HasMentionPrefix(_client.CurrentUser, ref argPos) ||
                context.Guild == null) ||
                message.Author.IsBot)
                return;

            string channelName = message.Channel.Name;
            if (channelName == null)
                channelName = "Unknown Channel";
            string guildName = context.Guild?.Name;
            if (guildName == null)
                guildName = "Direct Message";

            var commandName = message.Content.Split(' ').FirstOrDefault().Trim();
            commandName = commandName.Substring(prefix.Length);
            if (context.Guild != null)
                try
                {
                    // Execute the command with the command context we just
                    // created, along with the service provider for precondition checks.
                    var commands = _commands.Search(context, argPos);
                    if (commands.IsSuccess)
                    {
                        var logMessage = $"Command Received: {message.Id} {guildName} {channelName} {message.Content}";
                        logger.LogInformation(logMessage);
                    }
                    var result = await _commands.ExecuteAsync(context, argPos, services);
                    if (!result.IsSuccess)
                    {
                        if (result.Error != CommandError.UnknownCommand)
                            logger.LogError(result.ErrorReason);
                    }
                    else
                    {
                        logger.LogInformation("Time Taken for message {message.Id}: {commandTimer.Elapsed}", message.Id, commandTimer.Elapsed);
                    }
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, ex.Message);
                }
        }
    }
}
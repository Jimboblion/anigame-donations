﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Linq.Expressions;

namespace Jimboblion.Anigame.Donations
{
    public static class ContextExtensions
    {
        public static string FieldName<T, TKey>(this DbSet<T> dbset, Expression<Func<T, TKey>> property) where T: class
        {
            MemberExpression member = property.Body as MemberExpression;
            if (member == null)
                throw new Exception("Garbage Code");
            var storeObject = StoreObjectIdentifier.Table(dbset.EntityType.GetTableName(), dbset.EntityType.GetSchema());
            return dbset.EntityType.GetProperty(member.Member.Name).GetColumnName(storeObject);
        }
    }
}

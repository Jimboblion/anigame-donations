﻿using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jimboblion.Anigame.Donations
{
    public interface IDonationCache
    {
        IEnumerable<BotDonation> Entries { get; }

        Task Add(SocketUserMessage message);        
        bool IsDonation(SocketUserMessage message);
        Task AddPending(PendingDonation pendingDonation);
        Task Remove(BotDonation cacheEntry);
    }
}
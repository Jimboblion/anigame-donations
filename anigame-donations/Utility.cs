﻿using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jimboblion.Anigame.Donations
{
    class Utility : IUtility
    {
        private readonly DonationDatabase database;
        private readonly Config config;

        public async Task<string> Prefix(SocketGuild guild)
        {
            var prefix = config.Prefix;
            if (guild != null)
            {
                try
                {
                    var guildConfig = await database.Guilds.FindAsync((long)(guild.Id));
                    if (guildConfig != null && guildConfig.Prefix != null)
                        prefix = guildConfig.Prefix;
                }
                catch
                {
                }
            }
            return prefix;
        }
        public Utility(DonationDatabase database, Config config)
        {
            this.database = database;
            this.config = config;
        }
    }
}

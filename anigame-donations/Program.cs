﻿using Discord;
using Discord.WebSocket;
using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace Jimboblion.Anigame.Donations
{
    class Program
    {
        public static DateTime Start { get; } = DateTime.UtcNow;

        public static async Task Main(string[] args)
        {
            Config config = new Config();
            string configPath = "config.json";
#if DEBUG
            var exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            configPath = Path.Combine(exePath, configPath);
#endif
            if (File.Exists(configPath))
                config = Newtonsoft.Json.JsonConvert.DeserializeObject<Config>(File.ReadAllText(configPath));

            var envToken = Environment.GetEnvironmentVariable("DiscordToken");
            if (!string.IsNullOrEmpty(envToken))
                config.Token = envToken;

            var client = new DiscordShardedClient();
            client.Log += ConsoleLog;


            await client.LoginAsync(TokenType.Bot, config.Token);
            await client.StartAsync();
            await new CommandHandler(client, config).InstallCommandsAsync();
            // Block this task until the program is closed.
            await Task.Delay(-1);
        }

        private static Task ConsoleLog(LogMessage arg)
        {
            switch (arg.Severity)
            {
                case LogSeverity.Info: Console.ForegroundColor = ConsoleColor.Green; break;
                case LogSeverity.Warning: Console.ForegroundColor = ConsoleColor.Yellow; break;
                case LogSeverity.Error: Console.ForegroundColor = ConsoleColor.Red; break;
                case LogSeverity.Debug: Console.ForegroundColor = ConsoleColor.Blue; break;
                case LogSeverity.Verbose: Console.ForegroundColor = ConsoleColor.Gray; break;
                case LogSeverity.Critical: Console.ForegroundColor = ConsoleColor.White; break;
            }
            Console.WriteLine(arg.Message);
            return Task.CompletedTask;
        }
    }
}
